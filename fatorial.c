#include "fatorial.h"

int fatorial(int x){
	if(x>0){
		int fatorial = 1;
		while(x){
			fatorial=fatorial*x;
			x--;
		}
		return fatorial;
	}
	return -1;
}